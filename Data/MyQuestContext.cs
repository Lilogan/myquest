using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using MyQuest.Models;


public class MyQuestContext : DbContext
{
    public MyQuestContext(DbContextOptions<MyQuestContext> options)
        : base(options)
    {
    }

    public DbSet<MyQuest.Models.Individu> Individu { get; set; }
    public DbSet<MyQuest.Models.Cours> Cours { get; set; }
    public DbSet<MyQuest.Models.Chapitre> Chapitre { get; set; }
    public DbSet<MyQuest.Models.Contenu> Contenu { get; set; }
    public DbSet<MyQuest.Models.Parcours> Parcours { get; set; }
    public DbSet<MyQuest.Models.Etape> Etape {get; set;}
    public DbSet<MyQuest.Models.Quiz> Quiz { get; set; }
    public DbSet<MyQuest.Models.Question> Question { get; set; }
    public DbSet<MyQuest.Models.Reponse> Reponse { get; set; }
    public DbSet<MyQuest.Models.Evaluation> Evaluation { get; set; }
    public DbSet<MyQuest.Models.Groupe> Groupe { get; set; }


    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<Individu>()
            .HasMany(i => i.Cours)
            .WithOne(c => c.Auteur);

        modelBuilder.Entity<Individu>()
            .HasMany(i => i.Chapitres)
            .WithOne(ch => ch.Auteur);

        modelBuilder.Entity<Individu>()
            .HasMany(i => i.Parcours)
            .WithOne(p => p.Auteur);

        modelBuilder.Entity<Individu>()
            .HasMany(i => i.Quiz)
            .WithOne(q => q.Auteur);

        modelBuilder.Entity<Individu>()
            .HasMany(i => i.Questions)
            .WithOne(qu => qu.Auteur);

        modelBuilder.Entity<Individu>()
            .HasMany(i => i.Intervenir)
            .WithMany(c => c.Intervenants)
            .UsingEntity<Intervention>(
                j => j
                .HasOne(it => it.Cours)
                .WithMany(c => c.Interventions)
                .HasForeignKey(it => it.CoursID),
                j => j
                .HasOne(it => it.Individu)
                .WithMany(i => i.Interventions)
                .HasForeignKey(it => it.IndividuID));

        modelBuilder.Entity<Individu>()
            .HasMany(i => i.Apprendre)
            .WithMany(c => c.Apprenants)
            .UsingEntity<Apprentissage>(
                j => j
                .HasOne(ap => ap.Cours)
                .WithMany(c => c.Apprentissages)
                .HasForeignKey(ap => ap.CoursID),
                j => j
                .HasOne(ap => ap.Individu)
                .WithMany(i => i.Apprentissages)
                .HasForeignKey(ap => ap.IndividuID));

        modelBuilder.Entity<Individu>()
            .HasMany(i => i.Suivre)
            .WithMany(p => p.Suiveurs)
            .UsingEntity<Suivi>(
                j => j
                .HasOne(s => s.Parcours)
                .WithMany(p => p.Suivis)
                .HasForeignKey(s => s.ParcoursID),
                j => j
                .HasOne(s => s.Individu)
                .WithMany(i => i.Suivis)
                .HasForeignKey(s => s.IndividuID));

        modelBuilder.Entity<Evaluation>()
            .HasOne(e => e.Quiz)
            .WithMany(q => q.Evaluations)
            .HasForeignKey(e => e.QuizID);

        modelBuilder.Entity<Evaluation>()
            .HasOne(e => e.Individu)
            .WithMany(i => i.Evaluations)
            .HasForeignKey(e => e.IndividuID);

        modelBuilder.Entity<Evaluation>()
            .HasAlternateKey(e => new {e.IndividuID, e.QuizID});

        modelBuilder.Entity<Cours>()
            .HasMany(c => c.Chapitres)
            .WithOne(ch => ch.Cours)
            .OnDelete(DeleteBehavior.Cascade);

        modelBuilder.Entity<Chapitre>()
            .HasMany(ch => ch.Contenus)
            .WithOne(co => co.Chapitre)
            .OnDelete(DeleteBehavior.Cascade);

        modelBuilder.Entity<Parcours>()
            .HasMany(p => p.Etapes)
            .WithOne(e => e.Parcours)
            .OnDelete(DeleteBehavior.Cascade);

        modelBuilder.Entity<Etape>()
            .HasMany(e => e.Chapitres)
            .WithMany(ch => ch.Etapes)
            .UsingEntity(j => j.ToTable("ChapitreEtape"));

        modelBuilder.Entity<Chapitre>()
            .HasOne(ch => ch.Quiz)
            .WithOne(q => q.Chapitre)
            .HasForeignKey<Quiz>(q => q.ChapitreID);

        modelBuilder.Entity<Quiz>()
            .HasMany(q => q.Questions)
            .WithOne(qu => qu.Quiz)
            .OnDelete(DeleteBehavior.Cascade);

        modelBuilder.Entity<Question>()
            .HasMany(qu => qu.Reponses)
            .WithOne(r => r.Question)
            .OnDelete(DeleteBehavior.Cascade);
        modelBuilder.Entity<Groupe>()
            .HasMany(g => g.Individus)
            .WithMany(i => i.Groupes)
            .UsingEntity(join => join.ToTable("Appartenir"));



    }


}
