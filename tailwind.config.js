module.exports = {
  purge: {
    // enabled: true,
    // content: ['./Pages/**/*.cshtml'],
  },
  darkMode: false, // or 'media' or 'class'
  theme: {
    backgroundColor: (theme) => ({
      ...theme('colors'),
      primary: '#2368a2',
      'dark-primary': '#1a4971',
    }),
    container: {
      center: true,
    },
    borderColor: (theme) => ({
      ...theme('colors'),
      primary: '#2368a2',
      'dark-primary': '#1a4971',
    }),
    textColor: (theme) => ({
      ...theme('colors'),
      primary: '#2368a2',
      'dark-primary': '#1a4971',
    }),
  },
  variants: {
    extend: {},
  },
  plugins: [],
};
