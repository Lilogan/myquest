using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MyQuest.Models
{
    public class Etape
    {
        public Etape()
        {
            Chapitres = new HashSet<Chapitre>();
        }

        [Key]
        public int ID { get; set; }
        public String Titre { get; set; }
        public int Numero { get; set; }
        [ForeignKey("Parcours")]
        public int ParcoursID { get; set; }
        public Parcours Parcours { get; set; }
        public ICollection<Chapitre> Chapitres { get; set; }

    }
}