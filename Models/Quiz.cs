using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MyQuest.Models
{
    public class Quiz
    {
        public Quiz()
        {
            Questions = new List<Question>();
            Evaluations = new HashSet<Evaluation>();
        }
        [Key]
        public int ID { get; set; }
        public string Titre { get; set; }
        [DataType(DataType.Date)]
        public DateTime Date_Creation { get; set; }
        public string Langue { get; set; }
        public bool Publique { get; set; }
        public int Max_Question { get; set; }
        [ForeignKey("Auteur")]
        public string AuteurID { get; set; }
        public Individu Auteur { get; set; }
        [ForeignKey("Chapitre")]
        public int ChapitreID { get; set;}
        public Chapitre Chapitre { get; set;}
        public ICollection<Question> Questions { get; set; }
        public ICollection<Evaluation> Evaluations { get; set; }
    }
}