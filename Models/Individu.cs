using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Security.Claims;

namespace MyQuest.Models
{
    public class Individu
    {
        public Individu()
        {
            Cours = new HashSet<Cours>();
            Chapitres = new HashSet<Chapitre>();
            Parcours = new HashSet<Parcours>();
            Quiz = new HashSet<Quiz>();
            Evaluations = new HashSet<Evaluation>();
            Questions = new HashSet<Question>();
            Apprendre = new HashSet<Cours>();
            Intervenir = new HashSet<Cours>();
            Groupes = new HashSet<Groupe>();

        }
        public Individu(ClaimsPrincipal User) : this()
        {
            ID = User.FindFirst(ClaimTypes.NameIdentifier)?.Value;
            Nom = User.FindFirst("name")?.Value;
            Email = User.FindFirst("preferred_username")?.Value;
        }

        [Key]
        public string ID { get; set; }
        public string Nom { get; set; }
        public string Email { get; set; }
        public ICollection<Cours> Cours { get; set; }
        public ICollection<Chapitre> Chapitres { get; set; }
        public ICollection<Parcours> Parcours { get; set; }
        public ICollection<Quiz> Quiz { get; set; }
        public ICollection<Evaluation> Evaluations { get; set; }
        public ICollection<Question> Questions { get; set; }
        public ICollection<Cours> Intervenir { get; set; }
        public List<Intervention> Interventions { get; set; }
        public ICollection<Cours> Apprendre { get; set; }
        public List<Apprentissage> Apprentissages { get; set; }
        public ICollection<Parcours> Suivre { get; set; }
        public List<Suivi> Suivis { get; set; }
        public ICollection<Groupe> Groupes { get; set; }



        // override object.Equals
        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType())
            {
                return false;
            }
            return ID.Equals((obj as Individu).ID);
        }

        public override int GetHashCode()
        {
            return ID.GetHashCode();
        }

    }


}