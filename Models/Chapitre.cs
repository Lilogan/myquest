using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace MyQuest.Models
{
    public class Chapitre
    {
        public Chapitre(){
            Contenus = new HashSet<Contenu>();
            Etapes = new HashSet<Etape>();
        }

        [Key]
        public int ID { get; set; }
        public String Libelle { get; set; }
        public String Titre { get; set; }

        [Range(1, int.MaxValue, ErrorMessage = "Entrez un nombre supérieur à {1}")]
        public int Numero { get; set; }
        [ForeignKey("Cours")]
        public int CoursID { get; set; }
        public Cours Cours { get; set; }
        [ForeignKey("Auteur")]
        public string AuteurID { get; set; }
        public Individu Auteur { get; set; }
        public Quiz Quiz { get; set; }
        public ICollection<Contenu> Contenus { get; set; }
        public ICollection<Etape> Etapes { get; set; }
    }
}