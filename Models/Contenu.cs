using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MyQuest.Models
{
    public class Contenu
    {
        [Key]
        public int ID { get; set; }
        public string Nom { get; set; }
        public string Url { get; set; }
        public string Type { get; set; }
        [ForeignKey("Chapitre")]
        public int ChapitreID { get; set;}
        public Chapitre Chapitre { get; set; }
    }
}
