using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace MyQuest.Models
{
    public class Parcours
    {
        public Parcours()
        {
            Publique = true;
            Etapes = new HashSet<Etape>();
            Suiveurs = new HashSet<Individu>();
        }

        [Key]
        public int ID { get; set; }
        public string Titre { get; set; }
        public string Libelle { get; set; }
        public bool Publique { get; set; }
        [ForeignKey("Auteur")]
        public string AuteurID { get; set; }
        public Individu Auteur { get; set; }
        public ICollection<Etape> Etapes { get; set; }
        public ICollection<Individu> Suiveurs { get; set; }
        public List<Suivi> Suivis { get; set; }
    }
}