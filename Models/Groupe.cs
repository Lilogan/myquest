using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MyQuest.Models
{
    public class Groupe
    {
        public Groupe (){
            Individus = new HashSet<Individu>();
        }

        [Key]
        public int ID { get; set; }
        public string Code { get; set; }
        public string Libelle { get; set; }
        public ICollection<Individu> Individus { get; set; }

    }
}