using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace MyQuest.Models
{
    public class Question
    {
        public Question()
        {
            Reponses = new List<Reponse>();
            Active = false;
        }

        [Key]
        public int ID { get; set; }
        [DataType(DataType.Date)]
        public DateTime Date_Creation { get; set; }
        public Difficulte Niveau { get; set; }
        public float Poids { get; set; }
        public string Enonce{ get; set; }
        public string Justification { get; set; }
        public TypeReponse Type_Reponse { get; set; }
        public float Max_Penalite { get; set; }
        public bool Active { get; set; }
        [NotMapped]
        public bool Erreur { get; set; }
        [ForeignKey("Auteur")]
        public string AuteurID { get; set; }
        public Individu Auteur { get; set; }
        [ForeignKey("Quiz")]
        public int QuizID { get; set; }
        public Quiz Quiz { get; set; }
        public IList<Reponse> Reponses { get; set; }

    }

    public enum Difficulte
    {
        Facile,
        Moyen,
        Difficile
    }

    public enum TypeReponse
    {
        Vrai_Faux,
        Une_Reponse,
        Multi_Reponse,
    }
}