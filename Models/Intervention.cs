using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Security.Claims;

namespace MyQuest.Models
{
    public class Intervention
    {
        [DataType(DataType.Date)]
        public DateTime Date_Debut { get; set; }
        [DataType(DataType.Date)]
        public DateTime Date_Fin { get; set; }
        [ForeignKey("Individu")]
        public string IndividuID { get; set; }
        public Individu Individu { get; set; }
        [ForeignKey("Cours")]
        public int CoursID { get; set; }
        public Cours Cours { get; set; }

    }
}