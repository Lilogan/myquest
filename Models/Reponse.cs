using System.ComponentModel.DataAnnotations.Schema;
namespace MyQuest.Models
{
    public class Reponse
    {
        public Reponse()
        {}
        public Reponse(string enonce, bool valide=false)
        {
            Valide = valide;
            Enonce = enonce;
        }
        public int ID { get; set; }
        public string Enonce { get; set; }
        public bool Valide { get; set; }
        public float Valeur { get; set; }
        [NotMapped]
        public bool Choisi { get; set; }
        [ForeignKey("Question")]
        public int QuestionID { get; set; }
        public Question Question { get; set; }
    }
}