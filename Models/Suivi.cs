using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MyQuest.Models
{
    public class Suivi
    {
        [DataType(DataType.Date)]
        public DateTime Date_Debut { get; set; }
        [DataType(DataType.Date)]
        public DateTime Date_Fin { get; set; }
        [ForeignKey("Individu")]
        public string IndividuID { get; set; }
        public Individu Individu { get; set; }
        [ForeignKey("Parcours")]
        public int ParcoursID { get; set; }
        public Parcours Parcours { get; set; }
    }
}