using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MyQuest.Models
{
    public class Cours
    {
        public Cours() {
            Publique = true;
            Intervenants = new HashSet<Individu>();
            Apprenants = new HashSet<Individu>();
            Chapitres = new HashSet<Chapitre>();
            
        }

        [Key]
        public int ID { get; set; }
        public string Libelle { get; set; }
        public string Titre { get; set; }
        public Boolean Publique { get; set; }
        [DataType(DataType.Date)]
        public DateTime Date_Debut { get; set; }
        [DataType(DataType.Date)]
        public DateTime Date_Fin { get; set; }
        [ForeignKey("Auteur")]
        public string AuteurID { get; set; }
        public Individu Auteur { get; set; }
        public ICollection<Chapitre> Chapitres { get; set; }
        public ICollection<Individu> Intervenants { get; set; }
        public List<Intervention> Interventions { get; set; }
        public ICollection<Individu> Apprenants { get; set; }
        public List<Apprentissage> Apprentissages { get; set; }


        

    }
}

