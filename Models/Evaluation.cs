using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Security.Claims;

namespace MyQuest.Models
{
    public class Evaluation
    {
        public Evaluation()
        {
            MeilleurScore = 0;
            NombrePassage = 0;
            DateHeure = DateTime.Now;
        }
        public Evaluation(ClaimsPrincipal User, Quiz Quiz): this()
        {
            QuizID = Quiz.ID;
            IndividuID = User.FindFirst(ClaimTypes.NameIdentifier).Value;
        }

        [Key]
        public int ID { get; set; }
        public DateTime DateHeure { get; set; }
        public int NombrePassage { get; set; }
        public float MeilleurScore { get; set; }
        [ForeignKey("Individu")]
        public string IndividuID { get; set; }
        public Individu Individu { get; set; }
        [ForeignKey("Quiz")]
        public int QuizID { get; set; }
        public Quiz Quiz { get; set; }


        public float UpdateScore(float score)
        {
            if (score > this.MeilleurScore)
            {
                MeilleurScore = score;
            }
            DateHeure = DateTime.Now;
            NombrePassage += 1;

            return MeilleurScore;
        }
    }
}
