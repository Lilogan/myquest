using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MyQuest.Models;

namespace MyQuest.Pages.Intervenant.Parcour
{
    public class ModifierModel : PageModel
    {
        private readonly MyQuestContext _context;

        public ModifierModel(MyQuestContext context)
        {
            _context = context;
        }

        [BindProperty]
        public Parcours Parcours { get; set; }
        public async Task<IActionResult> OnGetAsync(int? parcoursID)
        {
            if (parcoursID == null)
            {
                return NotFound();
            }

            Parcours = await _context.Parcours.FirstOrDefaultAsync(p => p.ID == parcoursID);

            if (Parcours == null)
            {
                return NotFound();
            }
            return Page();
        }


        public async Task<IActionResult> OnPostAsync(int parcoursID)
        {
            var parcoursToUpdate = await _context.Parcours.FindAsync(parcoursID);

            if (parcoursToUpdate == null)
            {
                return NotFound();
            }

            if(await TryUpdateModelAsync<Parcours>(
                parcoursToUpdate,
                "parcours",
                p => p.Titre, p => p.Publique))
            {
                await _context.SaveChangesAsync();
                return RedirectToPage("../Index");
            }

            return Page();

        }
    }
}
