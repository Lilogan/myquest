using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MyQuest.Models;


namespace MyQuest.Pages.Intervanant.Parcour
{
    public class AjouterModel : PageModel

    {
        private readonly MyQuestContext _context;

        public AjouterModel(MyQuestContext context)
        {
            _context = context;
        }

        [BindProperty]
        public Parcours Parcours { get; set; }

        public async Task<IActionResult> OnGetAsync(int? parcoursID, string erreur)
        {
            if (parcoursID == null)
            {
                return NotFound();
            }

            Parcours = await _context.Parcours
                                .Include(p => p.Suiveurs)
                                .AsNoTracking()
                                .FirstOrDefaultAsync(m => m.ID == parcoursID);

            if (parcoursID == null)
            {
                return NotFound();
            }

            return Page();
        }

        public async Task<IActionResult> OnPostAsync(int parcoursID, string emailString)
        {
            var parcours = await _context.Parcours.FirstOrDefaultAsync(m => m.ID == parcoursID);

            if (parcours==null)
            {
                return NotFound();
            }

            if(!string.IsNullOrEmpty(emailString)){
                string[] emails = emailString.Split(",");

                foreach(var email in emails){
                    var individu = await _context.Individu.FirstOrDefaultAsync(i => i.Email == email);
                    parcours.Suiveurs.Add(individu);
                }

                try
                {
                    await _context.SaveChangesAsync();
                }
                catch(DbUpdateException ex)
                {
                    RedirectToPage("./Ajouter",
                                    new { parcoursID = parcoursID, error = ex.ToString() });
                }
            }

            return RedirectToPage("./Ajouter",
                            new {parcoursID = parcoursID});
        }

        public async Task<IActionResult> OnPostSupprimer(int parcoursID, string individuID, string intervenant)
        {
            var parcours = await _context.Parcours.FindAsync(parcoursID);

            if(parcours == null){
                return NotFound();
            }

            var individu = await _context.Entry(parcours)
                            .Collection(p => p.Suiveurs)
                            .Query()
                            .FirstOrDefaultAsync(i => i.ID == individuID);

            individu.Suivre.Remove(parcours);

            await _context.SaveChangesAsync();

            return RedirectToPage("./Ajouter",
                            new {parcoursID = parcoursID});
        }
    }
}
