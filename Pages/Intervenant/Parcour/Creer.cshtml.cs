using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MyQuest.Models;

namespace MyQuest.Pages.Intervenant.Parcour
{
    public class CreerModel : PageModel
    {
        private readonly MyQuestContext _context;

        public CreerModel(MyQuestContext context)
        {
            _context = context;
        }

        public IActionResult OnGet()
        {
            return Page();
        }
        [BindProperty]
        public Parcours Parcours { get; set; }
        public async Task<IActionResult> OnPostAsync()
        {

            if (!ModelState.IsValid)
            {
                return Page();
            }
            var individu = new Individu(User);
            _context.Individu.Attach(individu);
            individu.Parcours.Add(Parcours);
            await _context.SaveChangesAsync();

            return RedirectToPage("../Index");
        }
    }
}