using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using MyQuest.Models;

namespace MyQuest.Pages.Intervenant.Parcour
{
    public class SupprimerModel : PageModel
    {
        private readonly MyQuestContext _context;
        private readonly ILogger<SupprimerModel> _logger;

        public SupprimerModel(MyQuestContext context, ILogger<SupprimerModel> logger)
        {
            _context = context;
            _logger = logger;
        }

        [BindProperty]
        public Parcours Parcours { get; set; }
        public string ErrorMessage { get; set; }

        public async Task<IActionResult> OnGetAsync(int? parcoursID, bool? saveChangesError = false)
        {
            if (parcoursID == null)
            {
                return NotFound();
            }

            Parcours = await _context.Parcours.FirstOrDefaultAsync(m => m.ID == parcoursID);

            if (Parcours == null)
            {
                return NotFound();
            }

            if (saveChangesError.GetValueOrDefault())
            {
                ErrorMessage = String.Format("La suppression de {0} à echouée.", parcoursID);
            }

            return Page();
        }

        public async Task<IActionResult> OnPostAsync(int parcoursID)
        {
            var parcours = await _context.Parcours.FindAsync(parcoursID);

            if (parcours == null)
            {
                return NotFound();
            }

            try
            {
                _context.Parcours.Remove(parcours);
                await _context.SaveChangesAsync();
                return RedirectToPage("../Index");
            }
            catch (DbUpdateException ex)
            {
                _logger.LogError(ex, ErrorMessage);
                return RedirectToPage("./Supprimer",
                                new { parcoursID, saveChangesError = true });
            }
        }
    }
}
