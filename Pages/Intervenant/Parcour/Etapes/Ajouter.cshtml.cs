using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using MyQuest.Models;
using Microsoft.AspNetCore.Mvc.Rendering;
namespace Myquest.Pages.Intervenant
{
    public class AjouterChapitresEtapesModel : PageModel
    {
        private readonly MyQuestContext _context;

        public AjouterChapitresEtapesModel(MyQuestContext context)
        {
            _context = context;
        }

        public Etape Etape { get; set; }
        public IList<Chapitre> Chapitres { get;set; }
        public IList<Cours> Cours {get; set;}

        public List<SelectListItem> Options { get; set; }
        public string TitreSort { get; set; }
        public string CurrentFilter { get; set; }
        public string CurrentSort { get; set; }

        public char indicateurTri{get; set;} = '↓';

        [BindProperty]
        public List<int> SelectedChapitres{get; set;}

        public async Task<IActionResult> OnGetAsync(int? etapeID,int? coursID, string sortOrder, string search)
        {
            if (etapeID == null)
            {
                return NotFound();
            }

            Etape = await _context.Etape.FindAsync(etapeID);

            if (Etape == null)
            {
                return NotFound();
            }

            TitreSort = String.IsNullOrEmpty(sortOrder) ? "titre_desc" : "";

            IQueryable<Chapitre> chapitresIQ = from ch in _context.Chapitre
                                                select ch;

            Cours = await _context.Cours.ToListAsync();

            //Les valeurs de la dropdownlist
            Options = _context.Cours.Select(c =>
                                  new SelectListItem
                                  {
                                      Value = c.ID.ToString(),
                                      Text =  c.Titre
                                  }).ToList();

            Options.Insert(0,new SelectListItem{Value = "-1", Text = "Tous"});

            //Rechercher un nom - Si vide on affiche tous les cours
            if (!string.IsNullOrEmpty(search))
            {
                chapitresIQ = chapitresIQ.Where(s => s.Titre.Contains(search));
            }
            //Filtrer par cours - Si null pas de filtre
            if (coursID >= 0)
            {
                chapitresIQ = chapitresIQ.Where(ch => ch.CoursID == coursID);
            }

            switch (sortOrder){
                case "titre_desc":
                    chapitresIQ = chapitresIQ.OrderByDescending(ch => ch.Titre);
                    indicateurTri = '↑';
                    break;
                default:
                    chapitresIQ = chapitresIQ.OrderBy(ch => ch.Titre);
                    indicateurTri = '↓';
                    break;
            }

            Chapitres = await chapitresIQ.Include(ch => ch.Cours).AsNoTracking().ToListAsync();

            return Page();
        }
        public async Task<IActionResult> OnPostAsync(int etapeID){
            if (!ModelState.IsValid)
            {
                return Page();
            }

            var etape = await _context.Etape.FindAsync(etapeID);


            foreach(var i in SelectedChapitres){
                var chapitre = await _context.Chapitre.FindAsync(i);
                etape.Chapitres.Add(chapitre);
            }

            await _context.SaveChangesAsync();

            return RedirectToPage("./Index", new {etapeID = etapeID});

        }


    }

}