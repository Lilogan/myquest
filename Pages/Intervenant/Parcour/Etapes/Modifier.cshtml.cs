using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MyQuest.Models;

namespace MyQuest.Pages.Intervenant.Parcour.Etapes
{
    public class ModifierModel : PageModel
    {
        private readonly MyQuestContext _context;

        public ModifierModel(MyQuestContext context)
        {
            _context = context;
        }

        [BindProperty]
        public Etape Etape { get; set; }

        public async Task<IActionResult> OnGetAsync(int? etapeID)
        {
            if (etapeID == null)
            {
                return NotFound();
            }

            Etape = await _context.Etape.FirstOrDefaultAsync(m => m.ID == etapeID);

            if (Etape == null)
            {
                return NotFound();
            }
            return Page();
        }


        public async Task<IActionResult> OnPostAsync(int etapeID)
        {
            var etapeToUpdate = await _context.Etape.FindAsync(etapeID);

            if (etapeToUpdate == null)
            {
                return NotFound();
            }

            if(await TryUpdateModelAsync<Etape>(
                etapeToUpdate,
                "etape",
                e => e.Titre, e => e.Numero))
            {
                await _context.SaveChangesAsync();
                return RedirectToPage("../Index",
                                new { parcoursID = etapeToUpdate.ParcoursID });
            }

            return Page();
        }

    }
}
