using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using MyQuest.Models;
using System.IO;

namespace MyQuest.Pages.Intervenant.Parcour.Etapes
{
    public class CreerModel : PageModel
    {
        private readonly MyQuestContext _context;

        public CreerModel(MyQuestContext context)
        {
            _context = context;
        }

        [BindProperty]
        public Etape Etape {get; set;}

        public async Task<IActionResult> OnGetAsync(int? parcoursID)
        {
            if (parcoursID == null)
            {
                return NotFound();
            }

            var parcours = await _context.Parcours.FindAsync(parcoursID);

            if (parcoursID == null)
            {
                return NotFound();
            }

            Etape = new Etape();
            Etape.ParcoursID = parcoursID.Value;
            Etape.Numero = _context.Entry(parcours)
                                .Collection(p => p.Etapes)
                                .Query()
                                .Count()+1;

            return Page();
        }
        // To protect from overposting attacks, see https://aka.ms/RazorPagesCRUD
        public async Task<IActionResult> OnPostAsync(int parcoursID)
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            var parcours = await _context.Parcours.FindAsync(parcoursID);
            parcours.Etapes.Add(Etape);
            await _context.SaveChangesAsync();

            return RedirectToPage("../Index", new { parcoursID = parcoursID });
        }
    }
}