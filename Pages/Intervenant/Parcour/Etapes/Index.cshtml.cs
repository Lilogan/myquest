using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using MyQuest.Models;

namespace MyQuest.Pages.Intervenant.Parcour.Etapes
{
    public class IndexModel : PageModel
    {
        private readonly MyQuestContext _context;

        public IndexModel(MyQuestContext context)
        {
            _context = context;
        }
        public IList<Chapitre> Chapitres { get;set; }
        public Etape Etape {get;set;}
        public async Task<IActionResult> OnGetAsync(int? etapeID)
        {
            if (etapeID == null)
            {
                return NotFound();
            }

            Etape = await _context.Etape
                            .Include(e => e.Chapitres)
                            .AsNoTracking()
                            .FirstOrDefaultAsync(i => i.ID == etapeID);

            if(Etape == null)
            {
                return NotFound();
            }

            Chapitres = Etape.Chapitres.OrderBy(ch => ch.CoursID).ToList();

            return Page();
        }

        public async Task<IActionResult> OnPostDelier(int etapeID, int chapitreID)
        {
            var etape = await _context.Etape
                            .Include(e => e.Chapitres)
                            .FirstOrDefaultAsync(e => e.ID == etapeID);

            if (etape == null)
            {
                return NotFound();
            }

            var chapitre = etape.Chapitres.FirstOrDefault(ch => ch.ID == chapitreID);

            etape.Chapitres.Remove(chapitre);

            await _context.SaveChangesAsync();

            return RedirectToPage("./Index",
                            new {etapeID = etapeID});
        }
    }

}