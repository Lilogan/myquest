using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using MyQuest.Models;

namespace MyQuest.Pages.Intervenant.Parcour.Etapes
{
    public class SupprimerModel : PageModel
    {
        private readonly MyQuestContext _context;
        private readonly ILogger<SupprimerModel> _logger;

        public SupprimerModel(MyQuestContext context, ILogger<SupprimerModel> logger)
        {
            _context = context;
            _logger = logger;

        }

        [BindProperty]
        public Etape Etape { get; set; }
        public string ErrorMessage { get; set; }

        public async Task<IActionResult> OnGetAsync(int? etapeID, bool? saveChangesError = false)
        {
            if (etapeID == null)
            {
                return NotFound();
            }

            Etape = await _context.Etape.FirstOrDefaultAsync(m => m.ID == etapeID);

            if (Etape == null)
            {
                return NotFound();
            }

            if (saveChangesError.GetValueOrDefault())
            {
                ErrorMessage = String.Format("La suppression de {0} à echouée.", etapeID);
            }

            return Page();
        }

        public async Task<IActionResult> OnPostAsync(int etapeID)
        {
            var etape = await _context.Etape.FindAsync(etapeID);

            if (etape == null)
            {
                return NotFound();
            }

            try
            {
                _context.Etape.Remove(etape);
                await _context.SaveChangesAsync();
                return RedirectToPage("../Index",
                                new { parcoursID = etape.ParcoursID });
            }
            catch (DbUpdateException ex)
            {
                _logger.LogError(ex, ErrorMessage);
                return RedirectToPage("./Supprimer",
                                new { etapeID, saveChangesError = true });
            }
        }
    }
}
