using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using MyQuest.Models;
namespace MyQuest.Pages.Intervenant.Parcour
{
    public class IndexModel : PageModel
    {
        private readonly MyQuestContext _context;

        public IndexModel(MyQuestContext context)
        {
            _context = context;
        }

        public Parcours Parcours { get; set; }
        public IList<Etape> Etapes { get; set; }

        public async Task<IActionResult> OnGetAsync(int? parcoursID)
        {
            if (parcoursID == null)
            {
                return NotFound();
            }

            Parcours = await _context.Parcours
                                .Include(p => p.Etapes)
                                .AsNoTracking()
                                .FirstOrDefaultAsync(p => p.ID == parcoursID);

            if (Parcours == null)
            {
                return NotFound();
            }

            Etapes = Parcours.Etapes.ToList();

            return Page();
        }
    }

}