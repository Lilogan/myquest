using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using MyQuest.Models;

namespace MyQuest.Pages.Intervanant.Groupes
{
    public class SupprimerModel : PageModel
    {
        private readonly MyQuestContext _context;

        public SupprimerModel(MyQuestContext context)
        {
            _context = context;
        }

        [BindProperty]
        public Groupe Groupe { get; set; }

        public async Task<IActionResult> OnGetAsync(int? groupeID)
        {
            if (groupeID == null)
            {
                return NotFound();
            }

            Groupe = await _context.Groupe.FirstOrDefaultAsync(m => m.ID == groupeID);

            if (Groupe == null)
            {
                return NotFound();
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync(int groupeID)
        {
            Groupe = await _context.Groupe.FindAsync(groupeID);

            if (Groupe != null)
            {
                _context.Groupe.Remove(Groupe);
                await _context.SaveChangesAsync();
            }

            return RedirectToPage("./Index");
        }
    }
}
