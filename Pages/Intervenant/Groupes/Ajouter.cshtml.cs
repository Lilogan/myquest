using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MyQuest.Models;


namespace MyQuest.Pages.Intervanant.Groupes
{
    public class AjouterModel : PageModel

    {

        private readonly MyQuestContext _context;

        public AjouterModel(MyQuestContext context)
        {
            _context = context;
        }

        [BindProperty]
        public Groupe Groupe { get; set; }
        public List<SelectListItem> Cours { get; set; }
        public string Message { get; set;}

        public async Task<IActionResult> OnGetAsync(int? groupeID, bool? succes, string email)
        {
            if (groupeID == null) {
                return NotFound();
            }

            Groupe = await _context.Groupe
                                .Include("Individus")
                                .AsNoTracking()
                                .FirstOrDefaultAsync(m => m.ID == groupeID);

            if (Groupe == null)
            {
                return NotFound();
            }

            var individu = new Individu(User);

            Cours = await _context.Cours
                                .Include(c => c.Auteur)
                                .Where(c => c.Auteur.ID == individu.ID)
                                .Select(c =>
                                new SelectListItem
                                {
                                    Value = c.ID.ToString(),
                                    Text = c.Titre
                                })
                                .AsNoTracking()
                                .ToListAsync();

            if (succes.HasValue)
            {
                if(succes.Value)    { Message = "Le groupe a bien été ajouter au cours."; }
                else                { Message = String.Format("L'email '{0}' n'existe pas", email); }
            }

            return Page();
        }

        public async Task<IActionResult> OnPostAsync(int groupeID, string emailString)
        {
            Groupe = await _context.Groupe
                                .Include("Individus")
                                .FirstOrDefaultAsync(m => m.ID == groupeID);

            if (Groupe==null)
            {
                return NotFound();
            }

            if(!string.IsNullOrEmpty(emailString)){
                string[] emails = emailString.Split(",");

                foreach(var email in emails){

                    var individu = await _context.Individu.FirstOrDefaultAsync(i => i.Email == email);

                    if(!Groupe.Individus.Contains(individu)) {
                        Groupe.Individus.Add(individu);
                    }
                    
                    if (individu == null)
                    {
                        return RedirectToPage("./Ajouter",
                                        new {groupeID = groupeID, succes = false, email = email});
                    }
                }
                await _context.SaveChangesAsync();
            }

            return RedirectToPage("./Ajouter",
                            new {groupeID = groupeID});
        }

        public async Task<IActionResult> OnPostSupprimer(int groupeID, string individuID)
        {
            Groupe = await _context.Groupe
                                .Include(g=>g.Individus)
                                .FirstOrDefaultAsync(m => m.ID == groupeID);

            if(Groupe == null){
                return NotFound();
            }
            var individu=Groupe.Individus.FirstOrDefault(i => i.ID == individuID);

            if(individu == null){
                return NotFound();
            }

            Groupe.Individus.Remove(individu);
            await _context.SaveChangesAsync();

            return RedirectToPage("./Ajouter",
                            new {groupeID = groupeID});
        }

        public async Task<IActionResult> OnPostAjoutCours(int groupeID, int coursID)
        {
            Groupe = await _context.Groupe
                                .Include(g=>g.Individus)
                                .FirstOrDefaultAsync(m => m.ID == groupeID);

            if(Groupe==null){
                return NotFound();
            }

            var cours = await _context.Cours
                            .Include(c => c.Apprenants)
                            .FirstOrDefaultAsync(c => c.ID == coursID);

            foreach (var individu in Groupe.Individus)
            {
                if(!cours.Apprenants.Contains(individu))
                {
                    cours.Apprenants.Add(individu);
                }
            }

            await _context.SaveChangesAsync();

            return RedirectToPage("./Ajouter",
                            new {groupeID = groupeID, succes = true});
        }
    }
}
