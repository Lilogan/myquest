using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MyQuest.Models;

namespace MyQuest.Pages.Intervenant.Chapitres
{
    public class ModifierModel : PageModel
    {
        private readonly MyQuestContext _context;

        public ModifierModel(MyQuestContext context)
        {
            _context = context;
        }

        [BindProperty]
        public Chapitre Chapitre { get; set; }
        public async Task<IActionResult> OnGetAsync(int? chapitreID)
        {
            if (chapitreID == null)
            {
                return NotFound();
            }

            Chapitre = await _context.Chapitre
                .Include("Cours")
                .FirstOrDefaultAsync(m => m.ID == chapitreID);

            if (Chapitre == null)
            {
                return NotFound();
            }
            return Page();
        }
        public async Task<IActionResult> OnPostAsync(int chapitreID)
        {
            var chapitreToUpdate = await _context.Chapitre
                .Include("Cours")
                .FirstOrDefaultAsync(m => m.ID == chapitreID);

            if (chapitreToUpdate == null)
            {
                return NotFound();
            }

            if(await TryUpdateModelAsync<Chapitre>(
                chapitreToUpdate,
                "chapitre",
                ch => ch.Titre, ch => ch.Numero))
            {
                await _context.SaveChangesAsync();
                return RedirectToPage("../Cour/Index",
                                new { coursID = chapitreToUpdate.Cours.ID });
            }

            return Page();
        }
    }
}
