using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using MyQuest.Models;

namespace MyQuest.Pages.Intervenant.Chapitres
{
    public class IndexModel : PageModel
    {
        private readonly MyQuestContext _context;

        public IndexModel(MyQuestContext context)
        {
            _context = context;
        }

        public Chapitre Chapitre { get; set; }
        public IList<Contenu> Contenu { get; set; }

        [BindProperty(SupportsGet=true)]
        public int? FromEtape { get; set; }

        public async Task<IActionResult> OnGetAsync(int? chapitreID)
        {
            if (chapitreID == null)
            {
                return NotFound();
            }

            Chapitre = await _context.Chapitre
                .Include(ch => ch.Contenus)
                .Include(ch => ch.Quiz)
                .ThenInclude(q => q.Questions)
                .AsSplitQuery()
                .OrderBy(ch => ch.ID)
                .AsNoTracking()
                .FirstOrDefaultAsync(ch => ch.ID == chapitreID.Value);

            if (Chapitre == null)
            {
                return NotFound();
            }

            Contenu = Chapitre.Contenus.ToList();

            return Page();
        }

        public async Task<IActionResult> OnPostSupprimer(int contenuID)
        {
            var contenu = await _context.Contenu.FindAsync(contenuID);

            if (contenu == null)
            {
                return NotFound();
            }

            _context.Contenu.Remove(contenu);
            await _context.SaveChangesAsync();

            return RedirectToPage("./Index");
        }
    }
}
