using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MyQuest.Models;

namespace MyQuest.Pages.Intervenant.Chapitres
{
    public class CreerModel : PageModel
    {
        private readonly MyQuestContext _context;

        public CreerModel(MyQuestContext context)
        {
            _context = context;
        }

        [BindProperty]
        public Chapitre Chapitre { get; set; }

        public async Task<IActionResult> OnGetAsync(int? coursID)
        {
            if (coursID == null)
            {
                return NotFound();
            }

            var cours = await _context.Cours.FindAsync(coursID);

            if (cours == null)
            {
                return NotFound();
            }

            Chapitre = new Chapitre();
            Chapitre.CoursID = cours.ID;
            Chapitre.Numero =   _context.Entry(cours)
                                .Collection(c => c.Chapitres)
                                .Query()
                                .Count()+1;

            return Page();
        }

        public async Task<IActionResult> OnPostAsync(int coursID)
        {
            var cours = await _context.Cours.FindAsync(coursID);

            if (cours == null)
            {
                return NotFound();
            }

            var emptyChapitre = new Chapitre();

            if(await TryUpdateModelAsync<Chapitre>(
                emptyChapitre,
                "chapitre",
                ch => ch.Titre, ch => ch.Libelle, ch => ch.Numero
            ))
            {
                var individu = new Individu(User);
                emptyChapitre.AuteurID = individu.ID;
                cours.Chapitres.Add(emptyChapitre);

                await _context.SaveChangesAsync();

                return RedirectToPage("../Cour/Index",
                                new {coursID = cours.ID});

            }

            return Page();

        }
    }
}
