using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using MyQuest.Models;

namespace MyQuest.Pages.Intervenant.Chapitres
{
    public class SupprimerModel : PageModel
    {
        private readonly MyQuestContext _context;
        private readonly ILogger<SupprimerModel> _logger;

        public SupprimerModel(MyQuestContext context, ILogger<SupprimerModel> logger)
        {
            _context = context;
            _logger = logger;

        }

        [BindProperty]
        public Chapitre Chapitre { get; set; }
        public string ErrorMessage { get; set; }

        public async Task<IActionResult> OnGetAsync(int? chapitreID, bool? saveChangesError = false)
        {
            if (chapitreID == null)
            {
                return NotFound();
            }

            Chapitre = await _context.Chapitre
                .Include("Contenus")
                .FirstOrDefaultAsync(ch => ch.ID == chapitreID);

            if (Chapitre == null)
            {
                return NotFound();
            }

            if (saveChangesError.GetValueOrDefault())
            {
                ErrorMessage = String.Format("La suppression de {0} à echouée.", chapitreID);
            }

            return Page();
        }

        public async Task<IActionResult> OnPostAsync(int? chapitreID)
        {
            if (chapitreID == null)
            {
                return NotFound();
            }

            var chapitre = await _context.Chapitre.FindAsync(chapitreID);

            if (Chapitre == null)
            {
                return NotFound();
            }

            try
            {
                _context.Chapitre.Remove(chapitre);
                await _context.SaveChangesAsync();
                return RedirectToPage("../Cour/Index",
                                new {coursID = chapitre.CoursID});
            }
            catch (DbUpdateException ex)
            {
                _logger.LogError(ex, ErrorMessage);
                return RedirectToPage("./Supprimer",
                        new { chapitreID, saveChangesError = true });
            }

            ;
        }
    }
}
