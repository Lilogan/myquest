using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using MyQuest.Models;

namespace MyQuest.Pages.Intervenant.Chapitres
{
    public class AjouterModel : PageModel
    {
        private readonly MyQuestContext _context;

        public AjouterModel(MyQuestContext context)
        {
            _context = context;
        }

        [BindProperty]
        public Contenu Contenu { get; set; }
        //public IFormFile Fichier { get; set; }

        public IActionResult OnGet(int? chapitreID)
        {
            if (chapitreID == null)
            {
                return NotFound();
            }

            Contenu = new Contenu();
            Contenu.ChapitreID = chapitreID.Value;

            return Page();
        }

        public async Task<IActionResult> OnPostAsync(int chapitreID)
        {
            // if(string.IsNullOrEmpty(Contenu.Url)) {
            //     var file = Path.Combine(_environment.ContentRootPath, "./wwwroot", File.FileName);
            //     using (var fileStream = new FileStream(file, FileMode.Create))
            //     {
            //         await File.CopyToAsync(fileStream);
            //     }
            //     Contenu.Url = "\\" + File.FileName;
            // }

            if (!ModelState.IsValid)
            {
                return Page();
            }

            var chapitre = await _context.Chapitre.FindAsync(chapitreID);
            chapitre.Contenus.Add(Contenu);
            await _context.SaveChangesAsync();

            return RedirectToPage("./Index", new { chapitreID = chapitreID });
        }
    }
}