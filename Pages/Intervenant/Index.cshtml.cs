using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using MyQuest.Models;

namespace MyQuest.Pages.Intervenant
{
    public class IndexModel : PageModel
    {
        private readonly MyQuestContext _context;

        public IndexModel(MyQuestContext context)
        {
            _context = context;
        }

        public IList<Cours> Cours { get; set; }
        public IList<Parcours> Parcours { get; set; }
        public IList<Groupe> Groupe { get;set; }
        public async Task OnGetAsync()
        {
            Cours = await _context.Cours.ToListAsync();
            Parcours = await _context.Parcours.ToListAsync();
            Groupe = await _context.Groupe.ToListAsync();
        }
    }
}
