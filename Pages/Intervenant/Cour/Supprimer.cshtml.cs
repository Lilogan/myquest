using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using MyQuest.Models;

namespace MyQuest.Pages.Intervenant.Cour
{
    public class SupprimerModel : PageModel
    {
        private readonly MyQuestContext _context;
        private readonly ILogger<SupprimerModel> _logger;

        public SupprimerModel(MyQuestContext context, ILogger<SupprimerModel> logger)
        {
            _context = context;
            _logger = logger;
        }

        [BindProperty]
        public Cours Cours { get; set; }
        public string ErrorMessage { get; set; }

        public async Task<IActionResult> OnGetAsync(int? coursID, bool? saveChangesError = false)
        {
            if (coursID == null)
            {
                return NotFound();
            }

            Cours = await _context.Cours
                            .Include("Chapitres")
                            .AsNoTracking()
                            .FirstOrDefaultAsync(c => c.ID == coursID);

            if (Cours == null)
            {
                return NotFound();
            }

            if (saveChangesError.GetValueOrDefault())
            {
                ErrorMessage = String.Format("La suppression de {0} à echouée.", coursID);
            }

            return Page();
        }

        public async Task<IActionResult> OnPostAsync(int coursID)
        {
            var cours = await _context.Cours.FindAsync(coursID);

            if (cours == null)
            {
                return NotFound();
            }

            try
            {
                _context.Cours.Remove(cours);
                await _context.SaveChangesAsync();
                return RedirectToPage("../Index");
            }
            catch (DbUpdateException ex)
            {
                _logger.LogError(ex, ErrorMessage);
                return RedirectToPage("./Supprimer",
                                new { coursID, saveChangesError = true });
            }

        }
    }
}
