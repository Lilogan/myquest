using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MyQuest.Models;

namespace MyQuest.Pages.Intervenant.Cour
{
    public class ModifierModel : PageModel
    {
        private readonly MyQuestContext _context;

        public ModifierModel(MyQuestContext context)
        {
            _context = context;
        }

        [BindProperty]
        public Cours Cours { get; set; }
        public async Task<IActionResult> OnGetAsync(int? coursID)
        {
            if (coursID == null)
            {
                return NotFound();
            }

            Cours = await _context.Cours.FirstOrDefaultAsync(m => m.ID == coursID);

            if (Cours == null)
            {
                return NotFound();
            }
            return Page();
        }


        public async Task<IActionResult> OnPostAsync(int coursID)
        {
            var coursToUpdate = await _context.Cours.FindAsync(coursID);

            if(coursToUpdate == null)
            {
                return NotFound();
            }

            if(await TryUpdateModelAsync<Cours>(
                coursToUpdate,
                "cours",
                c => c.Titre, c => c.Libelle, c => c.Publique))
            {
                await _context.SaveChangesAsync();
                return RedirectToPage("../Index");
            }

            return Page();
        }
    }
}
