using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using MyQuest.Models;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace MyQuest.Pages.Intervenant.Cour
{
    public class IndexModel : PageModel
    {
        private readonly MyQuestContext _context;

        public IndexModel(MyQuestContext context)
        {
            _context = context;
        }

        public Cours Cours { get; set; }
        public IList<Chapitre> Chapitres { get; set; }

        public async Task<IActionResult> OnGetAsync(int? coursID)
        {
            if (coursID == null)
            {
                return NotFound();
            }

            Cours = await _context.Cours.Include("Chapitres").AsNoTracking().FirstOrDefaultAsync(c => c.ID == coursID.Value);

            if(Cours == null)
            {
                return NotFound();
            }

            Chapitres = Cours.Chapitres.ToList();

            return Page();

        }
    }
}
