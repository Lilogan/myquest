using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MyQuest.Models;


namespace MyQuest.Pages.Intervanant.Cour
{
    public class AjouterModel : PageModel

    {
        private readonly MyQuestContext _context;

        public AjouterModel(MyQuestContext context)
        {
            _context = context;
        }

        [BindProperty]
        public Cours Cours { get; set; }

        public async Task<IActionResult> OnGetAsync(int? coursID, string erreur)
        {
            if (coursID == null)
            {
                return NotFound();
            }

            Cours = await _context.Cours
                                .Include(c => c.Apprenants)
                                .Include(c => c.Intervenants)
                                .OrderBy(c => c.ID)
                                .AsSplitQuery()
                                .AsNoTracking()
                                .FirstOrDefaultAsync(m => m.ID == coursID);

            if (coursID == null)
            {
                return NotFound();
            }

            return Page();
        }

        public async Task<IActionResult> OnPostAsync(int coursID, string emailString, string intervanant)
        {
            var cours = await _context.Cours.FirstOrDefaultAsync(m => m.ID == coursID);

            if (cours==null)
            {
                return NotFound();
            }

            if(!string.IsNullOrEmpty(emailString)){
                string[] emails = emailString.Split(",");

                foreach(var email in emails){

                    var individu = await _context.Individu.FirstOrDefaultAsync(i => i.Email == email);
                    if(!string.IsNullOrEmpty(intervanant))
                    {
                        cours.Intervenants.Add(individu);
                    }
                    else
                    {
                        cours.Apprenants.Add(individu);
                    }
                }
                try
                {
                    await _context.SaveChangesAsync();
                }
                catch(DbUpdateException ex)
                {
                    RedirectToPage("./Ajouter",
                                    new { coursID = coursID, error = ex.ToString() });
                }
            }

            return RedirectToPage("./Ajouter",
                            new {coursID = coursID});
        }

        public async Task<IActionResult> OnPostSupprimer(int coursID, string individuID, string intervenant)
        {
            var cours = await _context.Cours.FindAsync(coursID);

            if(cours == null){
                return NotFound();
            }

            Individu individu;

            if (!string.IsNullOrEmpty(intervenant))
            {
                individu = await _context.Entry(cours)
                                    .Collection(c => c.Intervenants)
                                    .Query()
                                    .FirstOrDefaultAsync(i => i.ID == individuID);

                individu.Intervenir.Remove(cours);
            }
            else
            {
                individu = await _context.Entry(cours)
                                    .Collection(c => c.Apprenants)
                                    .Query()
                                    .FirstOrDefaultAsync(i => i.ID == individuID);

                individu.Apprendre.Remove(cours);
            }

            await _context.SaveChangesAsync();

            return RedirectToPage("./Ajouter",
                            new {coursID = coursID});
        }
    }
}
