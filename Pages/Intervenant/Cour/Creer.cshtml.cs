using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using MyQuest.Models;

namespace MyQuest.Pages.Intervenant.Cour
{
    public class CreerModel : PageModel
    {
        private readonly MyQuestContext _context;

        public CreerModel(MyQuestContext context)
        {
            _context = context;
        }

        public IActionResult OnGet()
        {
            return Page();
        }

        [BindProperty]
        public Cours Cours { get; set; }


        // To protect from overposting attacks, see https://aka.ms/RazorPagesCRUD
        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            var individu = new Individu(User);
            _context.Attach(individu);
            individu.Cours.Add(Cours);
            individu.Intervenir.Add(Cours);
            await _context.SaveChangesAsync();

            return RedirectToPage("../Index");
        }
    }
}
