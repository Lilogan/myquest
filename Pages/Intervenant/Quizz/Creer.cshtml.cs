using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using MyQuest.Models;

namespace MyQuest.Pages.Intervanant.Quizz
{
    public class CreerModel : PageModel
    {
        private readonly MyQuestContext _context;

        public CreerModel(MyQuestContext context)
        {
            _context = context;
        }

        [BindProperty]
        public Quiz Quiz { get; set; }

        public IActionResult OnGet(int? chapitreID)
        {
            if(chapitreID == null)
            {
                return NotFound();
            }

            Quiz = new Quiz();
            Quiz.ChapitreID = chapitreID.Value;

            return Page();
        }

        public async Task<IActionResult> OnPostAsync(int chapitreID)
        {
            var chapitre = await _context.Chapitre.FindAsync(chapitreID);

            if (chapitre == null)
            {
                return NotFound();
            }

            var emptyQuiz = new Quiz();

            if(await TryUpdateModelAsync<Quiz>(
                emptyQuiz,
                "quiz",
                q => q.Titre, q => q.Max_Question, q => q.Publique
            ))
            {
                var individu = new Individu(User);
                emptyQuiz.AuteurID = individu.ID;
                emptyQuiz.Date_Creation = DateTime.Now;
                chapitre.Quiz = emptyQuiz;

                await _context.SaveChangesAsync();
                return RedirectToPage("../Chapitres/Index",
                                new {chapitreID = chapitre.ID});
            }

            return Page();
        }
    }
}
