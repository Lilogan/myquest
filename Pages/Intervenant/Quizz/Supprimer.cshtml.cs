using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using MyQuest.Models;

namespace MyQuest.Pages.Intervanant.Quizz
{
    public class SupprimerModel : PageModel
    {
        private readonly MyQuestContext _context;
        private readonly ILogger<SupprimerModel> _logger;

        public SupprimerModel(MyQuestContext context, ILogger<SupprimerModel> logger)
        {
            _context = context;
            _logger = logger;
        }

        [BindProperty]
        public Quiz Quiz { get; set; }
        public string ErrorMessage { get; set; }

        public async Task<IActionResult> OnGetAsync(int? quizID, bool? saveChangesError = false)
        {
            if (quizID == null)
            {
                return NotFound();
            }

            Quiz = await _context.Quiz.
                AsNoTracking().FirstOrDefaultAsync(m => m.ID == quizID);

            if (Quiz == null)
            {
                return NotFound();
            }

            if (saveChangesError.GetValueOrDefault())
            {
                ErrorMessage = String.Format("La suppression de {0} à echouée.", quizID);
            }

            return Page();
        }

        public async Task<IActionResult> OnPostAsync(int? quizID)
        {
            if (quizID == null)
            {
                return NotFound();
            }

            var quiz = await _context.Quiz.FindAsync(quizID);

            if (quiz == null)
            {
                return NotFound();
            }

            try
            {
                _context.Quiz.Remove(quiz);
                await _context.SaveChangesAsync();
                return RedirectToPage("../Chapitres/Index",
                                new {chapitreID = quiz.ChapitreID});
            }
            catch (DbUpdateException ex)
            {
                _logger.LogError(ex, ErrorMessage);
                return RedirectToPage("./Supprimer",
                                new { quizID , saveChangesError = true });

            }
        }
    }
}
