using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MyQuest.Models;

namespace MyQuest.Pages.Intervanant.Quizz
{
    public class ModifierModel : PageModel
    {
        private readonly MyQuestContext _context;

        public ModifierModel(MyQuestContext context)
        {
            _context = context;
        }

        [BindProperty]
        public Quiz Quiz { get; set; }

        public async Task<IActionResult> OnGetAsync(int? quizID)
        {
            if(quizID == null){
                return NotFound();
            }

            Quiz = await _context.Quiz.FindAsync(quizID);

            if(Quiz == null)
            {
                return NotFound();
            }

            return Page();
        }
        public async Task<IActionResult> OnPostAsync(int quizID)
        {
            var quiz = await _context.Quiz.FindAsync(quizID);

            if (quiz == null)
            {
                return NotFound();
            }

            if (await TryUpdateModelAsync<Quiz>(
               quiz,
               "quiz",
               q => q.Titre, q => q.Publique, q => q.Max_Question))
            {
                _context.Update(quiz);
                await _context.SaveChangesAsync();
                return RedirectToPage("../Chapitres/Index",
                                new {chapitreID = quiz.ChapitreID});
            }
            return Page();
        }
    }
}
