using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using MyQuest.Models;

namespace MyQuest.Pages.Intervanant.Quizz.Questions
{
    public class CreerModel : PageModel
    {
        private readonly MyQuestContext _context;

        public CreerModel(MyQuestContext context)
        {
            _context = context;
        }

        [BindProperty]
        public Question Question { get; set; }

        public IActionResult OnGet(int? quizID)
        {
            if(quizID == null){
                return NotFound();
            }

            Question = new Question();
            Question.QuizID = quizID.Value;

            return Page();
        }

        public async Task<IActionResult> OnPostAsync(int quizID)
        {
            var emptyQuestion = new Question();
            var errors = ModelState.Values.SelectMany(v => v.Errors);

            if(await TryUpdateModelAsync<Question>(
                emptyQuestion,
                "question",
                qu => qu.Enonce, qu => qu.Type_Reponse, qu => qu.Niveau
            ))
            {
                var quiz = await _context.Quiz.FindAsync(quizID);
                var user = new Individu(User);
                _context.Attach(user);
                emptyQuestion.Auteur = user;
                emptyQuestion.Date_Creation = DateTime.Now;

                if(emptyQuestion.Type_Reponse == TypeReponse.Vrai_Faux)
                {
                    emptyQuestion.Reponses.Add(new Reponse("Vrai", true));
                    emptyQuestion.Reponses.Add(new Reponse("Faux"));
                }
                else
                {
                    emptyQuestion.Reponses.Add(new Reponse("",true));
                    emptyQuestion.Reponses.Add(new Reponse(""));
                }

                quiz.Questions.Add(emptyQuestion);
                await _context.SaveChangesAsync();
            }


            return RedirectToPage("./Modifier",new{ questionID=emptyQuestion.ID });

        }
    }
}