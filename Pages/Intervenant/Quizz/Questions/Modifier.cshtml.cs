using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MyQuest.Models;

namespace MyQuest.Pages.Intervanant.Quizz.Questions
{
    public class ModifierModel : PageModel
    {
        private readonly MyQuestContext _context;

        public ModifierModel(MyQuestContext context)
        {
            _context = context;
        }

        [BindProperty]
        public Question Question { get; set; }


        public async Task<IActionResult> OnGetAsync(int? questionID)
        {
            if (questionID == null)
            {
                return NotFound();
            }

            Question = await _context.Question
                .Include("Reponses")
                .AsNoTracking()
                .FirstOrDefaultAsync(qu => qu.ID == questionID);

            if (Question == null)
            {
                return NotFound();
            }

            return Page();
        }

        public async Task<IActionResult> OnPostAsync(int questionID)
        {
            var question = await _context.Question
                .Include("Reponses")
                .AsNoTracking()
                .FirstOrDefaultAsync(qu => qu.ID == questionID);

            if (question == null)
            {
                return NotFound();
            }

            if(await TryUpdateModelAsync<Question>(
                question,
                "question",
                qu => qu.Enonce, qu => qu.Poids, qu => qu.Justification, qu => qu.Niveau, qu => qu.Max_Penalite, qu => qu.Active, qu => qu.Reponses))
            {
                _context.Update(question);
                await _context.SaveChangesAsync();
                return RedirectToPage("../Index",
                                new {quizID=question.QuizID});
            }

            return Page();

        }

        public async Task<PartialViewResult> OnPostAddReponsePartial(int questionID)
        {
            Question = await _context.Question
                .Include("Reponses")
                .FirstOrDefaultAsync(qu => qu.ID == questionID);
            Question.Reponses.Add(new Reponse());
            await _context.SaveChangesAsync();

            return Partial("_ListReponsePartial", this);

        }
        public async Task<PartialViewResult> OnPostRemoveReponsePartial(int questionID, int reponseIndex)
        {
            Question = await _context.Question
                .Include("Reponses")
                .FirstOrDefaultAsync(qu => qu.ID == questionID);
            Question.Reponses.RemoveAt(reponseIndex);
            await _context.SaveChangesAsync();

            return Partial("_ListReponsePartial", this);
        }
    }
}
