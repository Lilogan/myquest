using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using MyQuest.Models;

namespace MyQuest.Pages.Intervanant.Quizz
{
    public class IndexModel: PageModel
    {
        private readonly MyQuestContext _context;

        public IndexModel(MyQuestContext context)
        {
            _context = context;
        }


        public Quiz Quiz { get; set; }
        public List<Question> Questions { get; set; }

        public async Task<IActionResult> OnGetAsync(int? quizID)
        {
            if(quizID == null)
            {
                return NotFound();
            }

            Quiz = await _context.Quiz.FindAsync(quizID);

            if(Quiz == null)
            {
                return NotFound();
            }

            Questions = await _context.Entry(Quiz)
                        .Collection(q => q.Questions)
                        .Query()
                        .OrderBy(qu => qu.Date_Creation)
                        .Include(qu => qu.Reponses)
                        .AsNoTracking()
                        .ToListAsync();

            return Page();
        }

        public async Task<IActionResult> OnPostSupprimer(int questionID){

            var question = await _context.Question.FindAsync(questionID);

            if (question == null)
            {
                return NotFound();
            }

            _context.Question.Remove(question);
            await _context.SaveChangesAsync();

            return RedirectToPage("./Index");
        }
    }
}