using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using MyQuest.Models;
using Microsoft.AspNetCore.Authorization;
namespace MyQuest.Pages.Apprenant
{
    [Authorize]
    public class IndexModel : PageModel
    {
        private readonly MyQuestContext _context;

        public IndexModel(MyQuestContext context)
        {
            _context = context;
        }

        public IList<Cours> Cours { get; set; }
        public IList<Parcours> Parcours { get; set; }

        public async Task OnGetAsync()
        {

            Cours = await _context.Cours
                            .Include(c => c.Chapitres)
                            .AsNoTracking()
                            .ToListAsync();

            Parcours = await _context.Parcours
                            .Include(p => p.Etapes)
                            .ThenInclude(e => e.Chapitres)
                            .OrderBy(p => p.ID)
                            .AsSplitQuery()
                            .AsNoTracking()
                            .ToListAsync();
        }
    }
}
