using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Security.Claims;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MyQuest.Models;

namespace MyQuest.Pages.Apprenant
{
    [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
    public class PasserModel : PageModel
    {
        private readonly MyQuestContext _context;

        public PasserModel(MyQuestContext context)
        {
            _context = context;
        }

        public Quiz Quiz { get; set; }

        [BindProperty]
        public List<Question> Questions { get; set; }
        public Evaluation Evaluation { get; set; }
        public float? CurrentScore { get; set; }

        public async Task<IActionResult> OnGetAsync(int? quizID)
        {
            if (quizID == null)
            {
                return NotFound();
            }

            Quiz = await _context.Quiz.FindAsync(quizID);

            if(Quiz == null)
            {
                return NotFound();
            }

            var individu = new Individu(User);

            Evaluation = await _context.Entry(Quiz)
                        .Collection(q => q.Evaluations)
                        .Query()
                        .FirstOrDefaultAsync(e => e.IndividuID == individu.ID);

            if (Evaluation == null)
            {
                Evaluation = new Evaluation(User, Quiz);
                _context.Evaluation.Add(Evaluation);
                await _context.SaveChangesAsync();
            }

            var questions = await _context.Entry(Quiz)
                        .Collection(q => q.Questions)
                        .Query()
                        .Where(qu => qu.Active)
                        .Include(qu => qu.Reponses)
                        .AsNoTracking()
                        .ToListAsync();

            Questions = questions
                        .OrderBy(qu => Guid.NewGuid())
                        .Take(Quiz.Max_Question)
                        .ToList();

            return Page();
        }

        public async Task<IActionResult> OnPostAsync(int quizID)
        {
            Quiz = await _context.Quiz.FindAsync(quizID);

            if(Quiz == null)
            {
                return NotFound();
            }

            var data = new List<Question>();
            var individu = new Individu(User);

            CurrentScore = 0;

            Evaluation = await _context.Entry(Quiz)
                        .Collection(q => q.Evaluations)
                        .Query()
                        .FirstOrDefaultAsync(e => e.IndividuID == individu.ID);

            foreach (var question in Questions)
            {
                float scoreQuestion = 0;
                var questionOrigin = await _context.Question
                                            .Include(qu => qu.Reponses)
                                            .AsNoTracking()
                                            .FirstOrDefaultAsync(qu => qu.ID == question.ID);

                foreach (var reponse in question.Reponses)
                {
                    var reponseOrigin = questionOrigin.Reponses.FirstOrDefault(r => r.ID == reponse.ID);

                    if(!reponse.Choisi) { continue; }

                    if(reponseOrigin.Valide) { scoreQuestion += reponseOrigin.Valeur; }
                    else
                    {
                        scoreQuestion -= reponseOrigin.Valeur;
                        questionOrigin.Erreur = true;
                    }

                }

                if(questionOrigin.Poids < scoreQuestion)
                {
                    CurrentScore += questionOrigin.Poids;
                    continue;
                }
                if(questionOrigin.Max_Penalite > scoreQuestion)
                {
                    CurrentScore -= questionOrigin.Max_Penalite;
                    continue;
                }

                data.Add(questionOrigin);
                CurrentScore += scoreQuestion;
            }

            Evaluation.UpdateScore(CurrentScore.Value);

            await _context.SaveChangesAsync();

            Questions = data;

            return Page();
        }

    }
}